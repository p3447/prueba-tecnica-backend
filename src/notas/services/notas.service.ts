import { Injectable, NotFoundException } from '@nestjs/common';
import { CrearNotaDto, ActualizarNotaDto } from '../dto/crear-notas.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Notas } from '../entities/notas.entity';
import { Repository } from 'typeorm';
import { UsersService } from 'src/usuarios/services/usuarios.service';

@Injectable()
export class NotasService {
  constructor(
    @InjectRepository(Notas)
    private NotasRepository: Repository<Notas>,
    private usersService: UsersService,
  ) {}

  async findAll() {
    const notas = await this.NotasRepository.find({
      relations: ['usuario'],
    });
    return notas;
  }

  async findOne(id: number) {
    const nota = await this.NotasRepository.findOne(id, {
      relations: ['usuario'],
    });

    if (!nota) {
      throw new NotFoundException(`Nota no encontrado...`);
    }

    return nota;
  }

  async create(data: CrearNotaDto) {
    try {
      const nuevaNota = this.NotasRepository.create(data);

      if (data.usuarioId) {
        const usuario = await this.usersService.findOne(data.usuarioId);
        nuevaNota.usuario = usuario;
      }

      const nota = await this.NotasRepository.save(nuevaNota);
      return nota;
    } catch (error) {
      console.log(error.sqlMessage);
      console.log(error);
      throw new NotFoundException(error.sqlMessage);
    }
  }

  async update(id: number, changes: ActualizarNotaDto) {
    const nota = await this.NotasRepository.findOne(id);

    if (changes.usuarioId) {
      const usuario = await this.usersService.findOne(changes.usuarioId);
      nota.usuario = usuario;
    }

    if (!nota) {
      throw new NotFoundException(`Nota no encontrada...`);
    }

    this.NotasRepository.merge(nota, changes);
    return this.NotasRepository.save(nota);
  }

  async remove(id: number) {
    const nota = await this.NotasRepository.delete(id);

    if (!nota) {
      throw new NotFoundException(`Nota no encontrada...`);
    }

    return nota.affected > 0;
  }
}
