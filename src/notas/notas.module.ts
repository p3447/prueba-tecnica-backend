import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';

import { NotasController } from './controllers/notas.controller';
import { NotasService } from './services/notas.service';
import { Notas } from './entities/notas.entity';

import { UsersModule } from 'src/usuarios/users.module';


@Module({
  imports: [
    PassportModule,
    TypeOrmModule.forFeature([Notas]),
    UsersModule,
  ],
  controllers: [NotasController],
  providers: [NotasService],
  exports: [NotasService],
})
export class NotasModule {}
