import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Usuarios } from 'src/usuarios/entities/usuarios.entity';
import { Comentarios } from 'src/comentarios/entities/comentarios.entity';

@Entity()
export class Notas {
  @PrimaryGeneratedColumn()
  idnota: number;

  @Column()
  titulo: string;

  @Column({ type: 'text' })
  descripcion: string;

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  fechapublicacion: Date;

  @Column()
  estatus: boolean;

  @ManyToOne(() => Usuarios, (usuarios) => usuarios.notas, { nullable: false })
  usuario: Usuarios;

  @OneToMany(() => Comentarios, (comentarios) => comentarios.notas)
  comentarios: Comentarios[];
}
