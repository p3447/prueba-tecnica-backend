import {
  IsString,
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsPositive,
} from 'class-validator';
import { PartialType, ApiProperty } from '@nestjs/swagger';

export class CrearNotaDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly titulo: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly descripcion: string;

  @IsBoolean()
  @IsNotEmpty()
  @ApiProperty()
  readonly estatus: boolean;

  @IsNumber()
  @IsPositive()
  @IsNotEmpty()
  @ApiProperty()
  readonly usuarioId: number;
}

export class ActualizarNotaDto extends PartialType(CrearNotaDto) {}
