import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';

import { UsersService } from '../../usuarios/services/usuarios.service';
import { Usuarios } from 'src/usuarios/entities/usuarios.entity';
import { PayloadToken } from '../models/token.model';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(correo: string, contraseña: string) {
    const user = await this.usersService.findByEmail(correo);
    if (user) {
      const isMatch = await bcrypt.compare(contraseña, user.contraseña);
      if (isMatch) {
        return user;
      }
    }
    return null;
  }

  generateJWT(user: Usuarios) {
    const payload: PayloadToken = {
      sub: user.id,
      role: user.rol,
    };

    const {contraseña, ...userPayload} = user;

    return {
      acces_token: this.jwtService.sign(payload),
      userPayload,
    };
  }
}
