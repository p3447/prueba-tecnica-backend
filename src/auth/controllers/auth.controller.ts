import { Controller, Post, Req, UseGuards } from '@nestjs/common';
import { Request } from 'express';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

import { AuthService } from '../services/auth.service';
import { Usuarios } from 'src/usuarios/entities/usuarios.entity';

@ApiTags('Autentificación')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(AuthGuard('local'))
  @Post('login')
  @ApiOperation({
    summary: `{
    *correo: "tu_correo@gmail.com",
    *contraseña: "tu_contraseña"}`,
  })
  login(@Req() req: Request) {
    const user = req.user as Usuarios;
    console.log(user)
    return this.authService.generateJWT(user);
  }
}
