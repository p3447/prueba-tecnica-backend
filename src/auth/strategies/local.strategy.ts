import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';

import { AuthService } from './../services/auth.service';
import { Strategy } from 'passport-local';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy, 'local') {
  constructor(private authService: AuthService) {
    super({
      usernameField: 'correo',
      passwordField: 'contraseña',
    });
  }

  async validate(correo: string, contraseña: string) {
    const user = await this.authService.validateUser(correo, contraseña);
    console.log('User Local strategy', user);
    if (!user) {
      throw new UnauthorizedException('not allow');
    }

    return user;
  }
}
