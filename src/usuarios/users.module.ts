import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';

import { UsuariosController } from './controllers/usuarios.controller';
import { UsersService } from './services/usuarios.service';
import { Usuarios } from './entities/usuarios.entity';

import { PersonalModule } from 'src/personal/personal.module';

@Module({
  imports: [
    PassportModule,
    TypeOrmModule.forFeature([Usuarios]),
    PersonalModule,
  ],
  controllers: [UsuariosController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
