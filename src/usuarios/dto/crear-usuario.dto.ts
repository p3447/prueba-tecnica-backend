import {
  IsString,
  IsBoolean,
  IsNotEmpty,
  IsEmail,
  IsNumber,
  IsPositive,
  IsOptional,
} from 'class-validator';
import { PartialType, ApiProperty } from '@nestjs/swagger';

export class CreatUsuarioDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly nombreusuario: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly rol: string;

  @IsNumber()
  @IsOptional()
  @IsNotEmpty()
  @ApiProperty()
  readonly personalId: number;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly correo: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly contraseña: string;
}

export class ActualizarUsuarioDto extends PartialType(CreatUsuarioDto) {}
