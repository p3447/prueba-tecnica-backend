import { Injectable, NotFoundException } from '@nestjs/common';
import {
  CreatUsuarioDto,
  ActualizarUsuarioDto,
} from '../dto/crear-usuario.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Usuarios } from '../entities/usuarios.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { PersonalService } from 'src/personal/services/personal.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Usuarios)
    private userRepository: Repository<Usuarios>,
    private personalService: PersonalService,
  ) {}

  async findAll() {
    const usuarios = await this.userRepository.find({
      relations: ['notas', 'personal'],
    });
    return usuarios;
  }

  async findOne(id: number) {
    const user = await this.userRepository.findOne(id, {
      relations: ['notas', 'personal'],
    });

    if (!user) {
      throw new NotFoundException(`Usuario no encontrado...`);
    }
    return user;
  }

  async findByEmail(correo: string) {
    console.log({correo})
    const user = await this.userRepository.findOne({ where: { correo } });
    if (!user) {
      throw new NotFoundException(`Usuario o contraseña incorrectos...`);
    }
    return user;
  }

  async create(data: CreatUsuarioDto) {
    try {
      const newUser = this.userRepository.create(data);

      if (data.personalId) {
        const personal = await this.personalService.findOne(data.personalId);
        newUser.personal = personal;
      }

      const hashPassword = await bcrypt.hash(data.contraseña, 10);
      newUser.contraseña = hashPassword;
      const user = await this.userRepository.save(newUser);
      return user;
    } catch (error) {
      console.log(error.sqlMessage);
      console.log(error);
      throw new NotFoundException(error.sqlMessage);
    }
  }

  async update(id: number, changes: ActualizarUsuarioDto) {
    const user = await this.userRepository.findOne(id);

    if (!user) {
      throw new NotFoundException(`Usuario no encontrado...`);
    }

    if (changes.personalId) {
      const personal = await this.personalService.findOne(changes.personalId);
      user.personal = personal;
    }
    this.userRepository.merge(user, changes);
    return this.userRepository.save(user);
  }

  async remove(id: number) {
    const resUser = await this.userRepository.delete(id);

    if (!resUser) {
      throw new NotFoundException(`Usuario no encontrado...`);
    }

    return resUser.affected > 0;
  }
}
