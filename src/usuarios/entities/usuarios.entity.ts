import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  Unique,
  OneToMany,
  OneToOne,
  JoinColumn,
} from 'typeorm';

import { Notas } from 'src/notas/entities/notas.entity';
import { Comentarios } from 'src/comentarios/entities/comentarios.entity';
import { Personal } from 'src/personal/entities/personal.entity';
import { Transform } from 'class-transformer';

@Unique('my_unique_constraint', ['correo'])
@Entity()
export class Usuarios {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombreusuario: string;

  @Column({ type: 'varchar', length: 30 })
  rol: string;

  @Column()
  correo: string;

  @Column({ type: 'varchar', length: 30 })
  contraseña: string;

  @OneToMany(() => Notas, (notas) => notas.usuario, { nullable: true })
  @Transform(({ value }) =>
    value.map(({ idnota, titulo }: Notas) => ({
      idnota,
      titulo,
    })),
  )
  notas: Notas[];

  @OneToMany(() => Comentarios, (comentarios) => comentarios.usuario)
  comentarios: Comentarios[];

  @OneToOne(() => Personal, (personal) => personal.usuarios, {
    nullable: true,
  })
  @JoinColumn({
    name: 'personalId',
  })
  personal: Personal;
}
