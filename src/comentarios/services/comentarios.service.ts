import { Injectable, NotFoundException } from '@nestjs/common';
import {
  CrearComentariosDto,
  ActualizarComentariosDto,
} from '../dto/crear-comentarios.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Comentarios } from '../entities/comentarios.entity';
import { Repository } from 'typeorm';
import { UsersService } from 'src/usuarios/services/usuarios.service';
import { NotasService } from 'src/notas/services/notas.service';
@Injectable()
export class ComentariosService {
  constructor(
    @InjectRepository(Comentarios)
    private ComentariosRepository: Repository<Comentarios>,
    private usersService: UsersService,
    private notasService: NotasService,
  ) {}

  async findAll() {
    const comentarios = await this.ComentariosRepository.find({
      relations: ['usuario', 'notas' , 'comentarios'],
    });
    return comentarios;
  }

  async findOne(id: number) {
    const comentario = await this.ComentariosRepository.findOne(id, {
      relations: ['usuario', 'notas'],
    });

    if (!comentario) {
      throw new NotFoundException(`Comentario no encontrado...`);
    }

    return comentario;
  }

  async create(data: CrearComentariosDto) {
    try {
      const nuevaComentario = this.ComentariosRepository.create(data);

      if (data.usuarioId) {
        const usuario = await this.usersService.findOne(data.usuarioId);
        nuevaComentario.usuario = usuario;
      }

      if (data.notaId) {
        const notas = await this.notasService.findOne(data.notaId);
        nuevaComentario.notas = notas;
      }

      if (data.comentarioId) {
        const comentarioPadre = await this.ComentariosRepository.findOne(data.comentarioId);
        console.log("comentarioPadre" , comentarioPadre);
        nuevaComentario.comentario = comentarioPadre;
      }

      

      const comentario = await this.ComentariosRepository.save(nuevaComentario);
      return comentario;
    } catch (error) {
      console.log(error.sqlMessage);
      console.log(error);
      throw new NotFoundException(error.sqlMessage);
    }
  }

  async update(id: number, changes: ActualizarComentariosDto) {
    const comentario = await this.ComentariosRepository.findOne(id);

    if (changes.usuarioId) {
      const usuario = await this.usersService.findOne(changes.usuarioId);
      comentario.usuario = usuario;
    }

    if (!comentario) {
      throw new NotFoundException(`Comentario no encontrado...`);
    }

    this.ComentariosRepository.merge(comentario, changes);
    return this.ComentariosRepository.save(comentario);
  }

  async remove(id: number) {
    const comentario = await this.ComentariosRepository.delete(id);

    if (!comentario) {
      throw new NotFoundException(`Comentario no encontrado...`);
    }

    return comentario.affected > 0;
  }
}
