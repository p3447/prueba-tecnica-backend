import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
  JoinColumn,
} from 'typeorm';
import { Usuarios } from 'src/usuarios/entities/usuarios.entity';
import { Notas } from 'src/notas/entities/notas.entity';

@Entity()
export class Comentarios {
  @PrimaryGeneratedColumn()
  idcomentario: number;

  @Column()
  descripcion: string;

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  fechapublicacion: Date;

  @Column()
  estatus: boolean;

  @ManyToOne(() => Usuarios, (usuarios) => usuarios.comentarios ,  {nullable: false} )
  usuario: Usuarios;

  @ManyToOne(() => Notas, (notas) => notas.comentarios , {nullable: false})
  @JoinColumn({
    name: 'notaid',
  })
  notas: Notas;

  @ManyToOne(() => Comentarios, (comentarios) => comentarios.comentarios, {
    nullable: true,
    onDelete: 'CASCADE'
  })
  @JoinColumn({
    name: 'comentarioId',
  })
  comentario: Comentarios;

  @OneToMany(() => Comentarios, (comentarios) => comentarios.comentario, {
    nullable: true,
  })
  comentarios: Comentarios[];
}
