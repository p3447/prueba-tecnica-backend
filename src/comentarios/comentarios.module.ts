import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';

import { ComentariosService } from './services/comentarios.service';
import { ComentariosController } from './controllers/comentarios.controller';
import { Comentarios } from './entities/comentarios.entity';

import { UsersModule } from 'src/usuarios/users.module';
import { Usuarios } from 'src/usuarios/entities/usuarios.entity';

import { Notas } from 'src/notas/entities/notas.entity';
import { NotasModule } from 'src/notas/notas.module';

@Module({
  imports: [
    PassportModule,
    TypeOrmModule.forFeature([Comentarios, Usuarios, Notas]),
    UsersModule,
    NotasModule,
  ],
  controllers: [ComentariosController],
  providers: [ComentariosService],
  exports: [ComentariosService],
})
export class ComentariosModule {}
