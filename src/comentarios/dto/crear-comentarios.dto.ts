import {
  IsString,
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsPositive,
  IsOptional,
} from 'class-validator';
import { PartialType, ApiProperty } from '@nestjs/swagger';

export class CrearComentariosDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly descripcion: string;

  @IsOptional()
  @IsNumber()
  @IsPositive()
  @IsNotEmpty()
  @ApiProperty()
  readonly comentarioId: number;

  @IsNumber()
  @IsPositive()
  @IsNotEmpty()
  @ApiProperty()
  readonly notaId: number;

  @IsNumber()
  @IsPositive()
  @IsNotEmpty()
  @ApiProperty()
  readonly usuarioId: number;

  @IsBoolean()
  @IsNotEmpty()
  @ApiProperty()
  readonly estatus: boolean;
}

export class ActualizarComentariosDto extends PartialType(
  CrearComentariosDto,
) {}
