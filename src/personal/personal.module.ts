import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';

import { PersonalService } from './services/personal.service';
import { PersonalController } from './controllers/personal.controller';
import { Personal } from './entities/personal.entity';

@Module({
  imports: [PassportModule, TypeOrmModule.forFeature([Personal])],
  controllers: [PersonalController],
  providers: [PersonalService],
  exports: [PersonalService],
})
export class PersonalModule {}
