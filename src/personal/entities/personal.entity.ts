import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  OneToMany,
  OneToOne,
} from 'typeorm';

import { Usuarios } from 'src/usuarios/entities/usuarios.entity';
@Entity()
export class Personal {
  @PrimaryGeneratedColumn()
  idpersonal: number;

  @Column()
  apepaterno: string;

  @Column()
  apematerno: string;

  @Column()
  nombre: string;

  @Column()
  direccion: string;

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  fechaingreso: Date;

  @OneToOne(() => Usuarios, (usuario) => usuario.personal, { nullable: true })
  usuarios: Usuarios;
}
