import {
  IsString,
  IsBoolean,
  IsNotEmpty,
  IsEmail,
  IsNumber,
  IsPositive,
} from 'class-validator';
import { PartialType, ApiProperty } from '@nestjs/swagger';

export class CrearPersonalDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly apepaterno: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly apematerno: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly nombre: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly direccion: string;
}

export class ActualizarPersonalDto extends PartialType(CrearPersonalDto) {}
