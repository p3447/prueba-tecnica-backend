import { Injectable, NotFoundException } from '@nestjs/common';
import {
  CrearPersonalDto,
  ActualizarPersonalDto,
} from '../dto/crear-personal.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Personal } from '../entities/personal.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

@Injectable()
export class PersonalService {
  constructor(
    @InjectRepository(Personal)
    private personalRepository: Repository<Personal>,
  ) {}

  async findAll() {
    const personal = await this.personalRepository.find();
    return personal;
  }

  async findOne(id: number) {
    const personal = await this.personalRepository.findOne(id);

    if (!personal) {
      throw new NotFoundException(`Usuario no encontrado...`);
    }
    return personal;
  }

  findByEmail(correo: string) {
    const personal = this.personalRepository.findOne({ where: { correo } });
    if (!personal) {
      throw new NotFoundException(`Usuario o contraseña incorrectos...`);
    }
    return personal;
  }

  async create(data: CrearPersonalDto) {
    try {
      const newPersonal = this.personalRepository.create(data);
      const personal = await this.personalRepository.save(newPersonal);
      return personal;
    } catch (error) {
      console.log(error.sqlMessage);
      console.log(error);
      throw new NotFoundException(error.sqlMessage);
    }
  }

  async update(id: number, changes: ActualizarPersonalDto) {
    const personal = await this.personalRepository.findOne(id);

    if (!personal) {
      throw new NotFoundException(`Usuario no encontrado...`);
    }

    this.personalRepository.merge(personal, changes);
    return this.personalRepository.save(personal);
  }

  async remove(id: number) {
    const personal = await this.personalRepository.delete(id);

    if (!personal) {
      throw new NotFoundException(`Usuario no encontrado...`);
    }

    return personal.affected > 0;
  }
}
