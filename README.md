## Requisitos
**Tener instalado Node.js .... https://nodejs.org/es/**

## Instalación
**Ingresar a la raiz del proyecto y correr el siguiente comando **

```bash
npm install
```


## Variables de entorno
**Ir al archivo .env y configurar sus variables de entorno**


```bash
# MYSQL
MYSQL_DATABASE=db_prueba_tecnica
MYSQL_USER=root
MYSQL_ROOT_PASSWORD=
MYSQL_PORT=3306
MYSQL_HOST=localhost

#API KEY GUARD
API_KEY='myKey'

# JSON WEB TOKEN
JWT_SECRET="myKey"
```

## Database
**Crear una base de datos llamada igual que su variable de entorno anterior , una vez instalada la aplicación corra el comando de inicio de la aplicacion y las tablas y relaciones se crearan por medio de TypeORM;**

```bash
npm run start:dev
```

## Documentación
**Una vez corriendo la aplicación dirigirse a la siguiente ruta en el navegador para revisar la documentacion de la API:  http://localhost:3000/docs/**


## Implementaciones
- Modulos
- Servicios
- Controladores
- DTO´S
- Authentificación con  Passport.js(JWT)
- TYPEORM (Entidades, relaciones)
- Variables de entorno
- Environment
- Pipes
- Swagger(Generar documentación)


